# PRISTHME

## Des regards décentrés pour éprouver notre réalité

PRISTHME est un espace d’expression, d’échange et d’élaboration collective adressé aux chercheurs, aux acteurs d’une éducation alternative et à toutes celles et ceux qui veulent entretenir le débat sur la production artistique et culturelle par une critique située. Articles,  podcasts,  ateliers, cycles de conférences sont au rendez-vous pour prolonger sur le plan éducatif l’expérimentation poétique initiée par POÉTISTHME. 

Parce que la question de l’art ne doit pas être réservée aux esthètes et encore moins à une aristocratie culturelle, nous croyons que chacun(e) doit disposer des moyens d’interroger son rapport à l’environnement artistique, et discuter hors des carcans idéologiques des incidences de la société sur la création artistique et inversement. Si notre intention est de décloisonner l’accès à la recherche ainsi que sa pratique, notre démarche est didactique et non démagogique. La recherche en art et sur l’art n’a jamais été populaire et nous n’avons pas la prétention qu’elle le devienne : nous n’avons aucune légitimité pour décréter ce qui serait bon ou non pour tous. En revanche, nous nous efforcerons d’étendre le domaine de l’éducation poétique à celles et ceux qui s’y intéressent et qui, par ce simple intérêt qui vaut toutes les « légitimités », doivent pouvoir l’aborder sans discrimination. **L’accès à la connaissance, les droits à l’interrogation et l’imagination, sont partie intégrante de la dignité humaine.** 
A l’instar de Louis Aragon, nous revendiquons un « droit de cité pour un réalisme expérimental, ce qui suppose un statut de l’artiste analogue à celui du chercheur scientifique ». Ainsi, nous ne céderons rien ni aux cuistres prônant l’élitisme culturel (le « bel esprit » est une pédanterie) ni aux dogmatismes. 

Les recherches que nous publions sont autant de témoignages qui doivent nous rappeler que l’art est le reflet de ce qui divise une société mais aussi de ce que nous avons en partage. 


## Notre vision du partage

Ce laboratoire en ligne vous est mis à disposition sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Le Collectif

POÉTISTHME est un collectif dédié à l’expérimentation poétique via l’édition, l’organisation d’évènements et d’interventions en milieu scolaire. Nous avons à coeur de promouvoir la poésie contemporaine et la faire vivre là où elle est nécessaire (c’est-à-dire partout) mais n’est pas nécessairement présente. Si vous désirez en savoir plus à notre sujet et nous rejoindre sur le chantier du rêve, nous vous invitons à consulter notre [Foyer des internets](https://poetisthme.cargo.site/). 

## Nous soutenir

Pour adhérer à notre association ou nous faire don d'une obole pour soutenir nos projets, c'est [ici](https://poetisthme.cargo.site/accueil)


